import numpy as np
import numpy.linalg as lin
import numpy.random as rnd
import cv2
import itertools

class Grid1d:
    def __init__(self, xs, ms, ks, r=0.01, a=1):
        self.grid_shape = xs.shape
        #Full state vector
        self.y = np.stack([np.zeros(xs.shape)]*2, axis=-1)
        self.y[:,0] = xs
        self.m = ms
        self.k = ks
        self.r = r
        self.a = a
    
    def f(self, y):
        tmp = self.y
        self.y = y
        fy = np.zeros_like(y)
        for i in range(self.grid_shape[0]):
            fy[i,1] = self.xbis(i)
        fy[:,0] = self.state(1)
        self.y = tmp
        return fy

    def x(self, i):
        return self.y[i,0]
    
    def xprim(self, i,j):
        return self.y[i,1]

    def frc(self, dst, src):
        y = self.y
        x = self.x
        dist = lin.norm(x(*dst)-x(*src))
        d = ((-x(*dst)+x(*src))/dist)*(dist-1)*self.k/self.m[dst[0],dst[1]]
        return d

    def xbis(self, i):
        y = self.y
        return 0
        if i==0 or j==0 or i == self.grid_shape[0]-1 or j == self.grid_shape[1]-1:
            return [0,0]
        else:
            force = self.frc((i,j),(i-1,j)) + self.frc((i,j),(i+1,j)) + \
                    self.frc((i,j),(i,j-1)) + self.frc((i,j),(i,j+1))
            vlen = lin.norm(self.xprim(i, j))
            if vlen != 0:
                force -= self.r*np.power(vlen, self.a)*self.xprim(i, j)/vlen 
            return force

    def state(self, order):
        return self.y[:, order]

    def set_new_state(self, state):
        self.y = state
        return

    def plot(self, canvas):
        canvas.plot_points(self.state(0), self.m)
        canvas.plot_vectors(self.state(0), self.state(1))
