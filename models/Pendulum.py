import numpy as np
from numpy import cos,sin

class Pendulum:
    g = 9.81
    def __init__(self, length=1, mass=1, omega=np.pi/4, r=0, k=100):
        self.m = mass
        self.L = length
        self.y = [omega, 0, 0, 0]
        self.r = r
        self.k = k
        self.rb_size = 5000
        self.rb_hist = np.array([[omega, length]]*self.rb_size)
        self.rb_index = 0

    def f(self, y):
        fy = np.zeros_like(self.y)
        fy[0]=y[1]
        fy[2]=y[3]
        fy[1] = -(self.g*np.sin(y[0])+2*y[1]*y[3])/(self.L + y[2])
        fy[3] = self.g*np.cos(y[0])+(self.L + y[2])*y[1]**2-self.k/self.m*y[2]
        return fy

    def state(self, order):
        return self.y[order],self.y[order+2]

    def set_new_state(self,state):
        self.y = state
        return

    def plot(self, canvas):
        o,d = self.state(0)
        d += self.L
        #print(o,d)
        x = np.sin(o)*d
        y = np.cos(o)*d
        point = np.array([x,y])
        self.rb_hist[self.rb_index] = [x,y]
        self.rb_index = (self.rb_index+1)%self.rb_size
        ppd = np.array([y, -x])/np.sqrt(y**2+x**2)
        npoint = point/np.linalg.norm(point)
        canvas.plot_line(point, -point, (50,50,50), 1)
        canvas.plot_cpoints(self.rb_hist,0.1, (200, 50,50))
        canvas.plot_points(point, np.array([self.m]))
        canvas.plot_vectors(point, ppd*self.y[1])
        canvas.plot_vectors(point, npoint*self.y[3])


class TrolleyPendulum:
    g = 9.81
    def __init__(self, length=1, mw=1, mk=1, theta=np.pi/4, r=0, k=10000, x=0):
        self.mw = mw
        self.mk = mk

        self.L = length
        self.y = [theta, 0,  x, 0]
        self.r = r
        self.k = k
        self.rb_size = 5000
        self.rb_hist = np.array([[sin(theta)*length+x, cos(theta)*length]]*self.rb_size)
        self.rb_index = 0

    def f2(self, y):
        fy = np.zeros_like(self.y)
        fy[0] = y[1]
        fy[2] = y[3]
        th = y[0]
        thp = y[1]
        x = y[2]
        xp = y[3]
        F = -1
        g = self.g
        L = self.L
        mk = self.mk
        mw = self.mw
        fy[1] = (cos(th)*(F+mk*L*sin(th)*thp**2)+g*sin(th)*(mk+mw))/(L*(mk*cos(th)**2-mw-mk))
        fy[3] = (F + mk*L*sin(th)*thp**2+mk*cos(th)*g*sin(th))/(mk*cos(th)**2-mw-mk)


    def f(self, y):
        fy = np.zeros_like(self.y)
        fy[0] = y[1]
        fy[2] = y[3]
        th = y[0]
        dth = y[1]
        x = y[2]
        dx = y[3]
        #print(y)
        A1 = self.mw + self.mk
        B1 = self.mk*self.L*cos(th)
        C1 = self.mk*self.L*dth**2*sin(th)-self.k*x
        A2 = self.L*np.cos(th)
        B2 = self.L**2
        C2 = -self.g*self.L*sin(th)
        #print(A1,A2, B1, B2, C1, C2)
        fy[1] = (C2-A2*C1/A1)/(B2-A2/A1*B1)
        fy[3] = (C1-B1*C2/B2)/(A1-B1/B2*A2)

        return fy

    def state(self, order):
        return self.y[order],self.y[order+2]

    def set_new_state(self,state):
        self.y = state
        return

    def plot(self, canvas):
        o,x = self.state(0)
        #print(o,x)
        px = np.sin(o)*self.L
        py = np.cos(o)*self.L
        print("th {}, dth {}, x {}, dx {}".format(*self.y))

        point = np.array([px,py])
        shift = np.array([x,0])

        self.rb_hist[self.rb_index] = point + shift
        self.rb_index = (self.rb_index+1)%self.rb_size
        ppd = np.array([py, -px])
        ppd = ppd/np.linalg.norm(ppd)
        #npoint = point/np.linalg.norm(point)
        canvas.plot_line(point+shift, -point, (50,50,50), 1)
        canvas.plot_cpoints(self.rb_hist,0.1, (160, 30,30))
        canvas.plot_points(point+shift, np.array([self.mk]))
        canvas.plot_cpoints(shift, self.mw,(160,100,20))
        canvas.plot_vectors(point+shift, ppd*self.y[1])
        canvas.plot_vectors(shift, np.array([1,0])*self.y[3])
