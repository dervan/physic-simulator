import numpy as np
import numpy.linalg as lin
import numpy.random as rnd
import cv2
import itertools
from models.Pendulum import Pendulum
from simulation_utils import *
xres = 800
yres = 800
unit = 400
canvas = Canvas(xres, yres, unit, xres/2, 0)

#canvas.set_clean()

i = 0
dt = 0.033
model = Pendulum(r=0.7, k=30, omega=0.1)

for points_rk4 in simulate_rk4(model, dt):
    canvas.clear()
    model.plot(canvas)
    print "Step " + str(i) + " (" + str(i*dt) + "s)"
    i+=1
    cv2.imshow("Pendulum", canvas.image())
    cv2.waitKey(1)
