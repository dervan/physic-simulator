import numpy as np
from numpy.linalg import norm
import cv2
from numpy import sqrt

def rotate(vector, angle):
    c, s = np.cos(angle), np.sin(angle)
    R = np.array([[c, -s], [s, c]])
    return np.dot(R,vector)

def ituple(a):
    return (int(a[0]), int(a[1]))

def simulate_rk4(model, dt=0.1):
    while True:
        k1 = model.f(model.y)
        k2 = model.f(model.y+0.5*dt*k1)
        k3 = model.f(model.y+0.5*dt*k2)
        k4 = model.f(model.y+dt*k3)
        y2 = model.y+dt*(1.0/6*k1 + 2.0/6*k2 + 2.0/6*k3 + 1.0/6*k4)
        model.set_new_state(y2)
        yield model.state(0)

class Canvas:
    def __init__(self, x, y, unit, xshift=0, yshift=0):
        self.clean = np.zeros((y, x, 3), np.uint8)
        self.img = self.clean.copy()
        self.unit = unit
        self.dx = xshift
        self.dy = yshift
    
    def toPixel(self, point):
        return (int(self.unit*point[0])+self.dx, int(self.unit*point[1])+self.dy)

    def set_clean(self):
        self.clean = self.img.copy()

    def clear(self):
        self.img = self.clean.copy()

    def plot_vectors(self, points, vectors, color=(20,12,210), width=1):
        for x,v in zip(points.reshape([-1,2]), vectors.reshape([-1,2])):
            v = v/8
            start = self.toPixel(x)
            end = self.toPixel(x+v)
            cv2.line(self.img, start, end, color, width)
            arr = v/norm(v)
            lv = norm(v)
            arr = arr * max(min(lv/5, 0.05), 0.01)
            cv2.line(self.img, self.toPixel(x+v-rotate(arr, 0.7)), end, color, width)
            cv2.line(self.img, self.toPixel(x+v-rotate(arr, -0.7)), end, color, width)
            cv2.line(self.img, start, end, color, width)


    def plot_line(self, points, vectors, color=(20,12,210), width=2):
        for x,v in zip(points.reshape([-1,2]), vectors.reshape([-1,2])):
            start = self.toPixel(x)
            end = self.toPixel(x+v)
            cv2.line(self.img, start, end, color, width)

    def plot_cpoints(self, points, size, color=(255,112,10)):
        for x in points.reshape([-1,2]):
            cv2.circle(self.img, self.toPixel(x), 3*int(sqrt(size)), color, -1)


    def plot_points(self, points, sizes):
        for x,r in zip(points.reshape([-1,2]), sizes.flatten()):
            cv2.circle(self.img, self.toPixel(x), 3*int(sqrt(r)), (255, 112,10), -1)

    def image(self):
        return self.img


