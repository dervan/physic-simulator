import numpy as np
import numpy.random as rnd
import cv2
from models.Grid1d import Grid1d
from simulation_utils import *

n = 5
xres = 1272
yres = 772

ds = rnd.random(n)+0.5
eq = np.zeros(n+1)
eq[1:] = np.cumsum(ds)
ms = rnd.random(n+1)+1
ms *= ms
ks = np.ones(n).astype('float64')*0.4
start = eq.copy()
eq[1] -= 0.2
ms[1] = 2
unit = yres/eq[-1]

canvas = Canvas(xres, yres, unit)
canvas.plot_cpoints(start, 2)
canvas.set_clean()

model = Grid1d(start, ms)

i = 0
for points_rk4 in simulate_rk4(model, dt):
    canvas.clear()
    model.plot(canvas)
    print "Step " + str(i) + " (" + str(i*dt) + "s)"
    i+=1
    cv2.imshow("weights", canvas.image())
    cv2.waitKey(1)
