import numpy as np
import numpy.linalg as lin
import numpy.random as rnd
import cv2
import itertools
from models.Grid2d import Grid2d
from simulation_utils import *

# Initialize grid
n = 16
m = 9
xres = 1272
yres = 772
unit_y = yres/m
unit_x = xres/n
unit = min(unit_y, unit_x)

xs = np.arange(0,0.01+n,1)
ys = np.arange(0,0.01+m,1)
grid = np.stack(np.meshgrid(xs, ys), axis=-1).astype('float64')
start = np.stack(np.meshgrid(xs, ys), axis=-1).astype('float64')
ms = rnd.random((m+1, n+1))+1
ms *= ms
ms = np.ones_like(ms).astype('float64')
start[4,4,1] -= 0.4
start[4,4,0] -= 0.4

step = 3.0/n
start[1,:n/2,1] -= np.arange(0, step*n/2,step)/2
start[1,-n/2-1:,1] -= np.arange(step*n/2+0.001, 0.0,-step)/2

#start[1,4:-4,1] -= 0.3
#start[1,8:-8,1] -= 0.2
# Image
canvas = Canvas(xres, yres, unit)
canvas.plot_cpoints(grid[:,:], 2, (0, 172,40))
canvas.set_clean()

i = 0
dt = 0.3
model = Grid2d(start, ms)

for points_rk4 in simulate_rk4(model, dt):
    canvas.clear()
    model.plot(canvas)
    print "Step " + str(i) + " (" + str(i*dt) + "s)"
    i+=1
    cv2.imshow("weights", canvas.image())
    cv2.waitKey(1)
