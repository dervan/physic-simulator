import numpy as np
import numpy.linalg as lin
import numpy.random as rnd
import cv2
import itertools
from models.Pendulum import TrolleyPendulum
from simulation_utils import *
xres = 1280
yres = 800
unit = 300
canvas = Canvas(xres, yres, unit, xres/2, yres/2)

#canvas.set_clean()

i = 0
dt = 0.01
model = TrolleyPendulum(k=100, theta=0.0, x=-0.50)
#model = TrolleyPendulum(k=10, theta=1.2, x=0.0)
#model = TrolleyPendulum(k=1, theta=1.6, x=0.0, mw=10)
#model = TrolleyPendulum(k=1000, theta=0.1, x=0.4, mw=100, mk=10)

for points_rk4 in simulate_rk4(model, dt):
    canvas.clear()
    model.plot(canvas)
    print "Step " + str(i) + " (" + str(i*dt) + "s)"
    i+=1
    cv2.imshow("Pendulum", canvas.image())
    if cv2.waitKey(3) == 27:
        cv2.destroyAllWindows()
        exit(0)
